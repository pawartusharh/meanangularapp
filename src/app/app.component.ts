import { Component } from '@angular/core';
import { UserService } from './services/userServices/userService.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'angularApp';

  // UserService to check user logged in
  constructor(public _userService: UserService){}
  
}
