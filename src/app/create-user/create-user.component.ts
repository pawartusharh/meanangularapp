import { Component, OnInit } from '@angular/core';
// import { FormGroup, FormControl, FormControlName} from '@angular/forms';
import { FormBuilder, Validators, FormGroup, FormArray } from '@angular/forms'; // dynamic controle step 1 (add FormArray)
import { forbiddenNameValidator } from '../shared/user-name.validator';
import { PasswordValidator } from '../shared/password.validator';
import { UserService } from '../services/userServices/userService.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.scss']
})
export class CreateUserComponent implements OnInit {
  submitted = false;
  errorMsg = '';
  createUserForm: FormGroup;

  // getter controle
  get userName() {
    return this.createUserForm.get('userName')
  }

  // getter controle
  get email() {
    return this.createUserForm.get('email')
  }

  // dynamic controle step 3
  get alternetEmails() {
    return this.createUserForm.get('alternetEmails') as FormArray;
  }
  
  // dynamic controle step 4
  addalternetEmail() {
    this.alternetEmails.push(this.fb.control(''));
  }
  
  // dynamic controle step 5 add button in html
  // dynamic controle step 6 iteret using ngFor in html

  constructor(private fb:FormBuilder, private _createUserService : UserService, private _router:Router) { }
  
  // createUserModel = new User2('tush', 'rob@test.com', 7979797979, 'default', 'morning', true, '123', '123')

  // commented & added formbuilder
  // createUserForm = new FormGroup({
  //   userName : new FormControl('Tushar'),
  //   password : new FormControl(''),
  //   confirmPassword : new FormControl(''),
  //   address : new FormGroup({
  //     city : new FormControl(''),
  //     state : new FormControl(''),
  //     postalCode : new FormControl('')
  //   })
  // });


  // commented & added inside ngOnInit
  // createUserForm = this.fb.group({
  //   userName: ['', [Validators.required, Validators.minLength(5), , forbiddenNameValidator(/password/)]],
  //   email: [''],
  //   subscribe: [false],
  //   password: ['', Validators.required],
  //   confirmPassword: ['', Validators.required],
  //   address: this.fb.group({
  //     city: [''],
  //     state: [''],
  //     postalCode: ['']
  //   })
  // }, {validator: PasswordValidator});

  ngOnInit(): void {
    // SIMPLE VALIDATION  Validators.required
    // CUSTOM VALIDATION  forbiddenNameValidator(/password/)
    // CROSS FEILD VALIDATION  {validator: PasswordValidator}
    this.createUserForm = this.fb.group({
      userName: ['', [Validators.required, Validators.minLength(5), forbiddenNameValidator(/password/)]],
      email: [''],
      subscribe: [false],
      password: ['', Validators.required],
      confirmPassword: ['', Validators.required],
      address: this.fb.group({
        city: [''],
        state: [''],
        postalCode: ['']
      }),
      alternetEmails: this.fb.array([]) // dynamic controle step 2
    }, {validator: PasswordValidator});
    
    // conditional validator
    this.createUserForm.get('subscribe').valueChanges
    .subscribe(checkedValue => {
      const email = this.createUserForm.get('email');
      if (checkedValue) {
        email.setValidators(Validators.required)
      } else {
        email.clearValidators();
      }
      email.updateValueAndValidity();
    })
  }

  createUser(createForm){
    this.submitted = false;

    this._createUserService.createUser(this.createUserForm.value)
      .subscribe(
        res => {
          localStorage.setItem('token', res.token)
          this._router.navigate(['/special'])
        },
        err => console.log('Error!', err)
      );
  }

  loadApiData(){
    // need all parameters
    // this.createUserForm.setValue({
    //   userName : "tush",
    //   password : "test",
    //   confirmPassword : "test",
    //   address : {
    //     city : "mycity",
    //     state : "mystate",
    //     postalCode : "123456"          
    //   }
    // })

    // can skip parameters
    this.createUserForm.patchValue({
      userName : "tusha",
      password : "test",
      confirmPassword : "test",
      address : {
        city : "mycity",
        state : "mystate",
        postalCode : "123456"          
      }
    })
  }

}
