import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/userServices/userService.service';
import { Route } from '@angular/compiler/src/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginUserData = { email: '', password: ''}
  constructor(private _http: UserService, private _router: Router) { }

  ngOnInit(): void {
  }

  loginUser(){
    this._http.loginUser(this.loginUserData)
      .subscribe(
        res => {
          localStorage.setItem('token', res.token)
          this._router.navigate(['/special'])
        },
        err => console.log('errrrr', err)
      )
  }

}
