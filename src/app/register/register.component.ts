import { Component, OnInit } from '@angular/core';
import { User } from './../user';
import { UserService } from '../services/userServices/userService.service';
import { Router } from '@angular/router';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  topics = ['Angular', 'React', 'Vue'];
  topicHasError = true;
  submitted = false;
  errorMsg = '';

  registerUserModel = new User('tush', 'rob@test.com', 7979797979, 'default', 'morning', true, '123')
  
  constructor(private _userService:UserService, private _router: Router){}
  
  validateTopic(value){
    if(value === 'default'){
      this.topicHasError = true;
    } else{
      this.topicHasError = false;
    }
  }
  
  ngOnInit(): void {
  }

  registerUser(){
    this.submitted = true;
    this._userService.enroll(this.registerUserModel)
      .subscribe(
        res => {
          localStorage.setItem('token', res.token);
          this._router.navigate(['/special'])
        },
        error => this.errorMsg = error.statusText
      )    
  }
}
