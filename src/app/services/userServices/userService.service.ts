import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { User } from './../../user';
import { catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  // making url private will throw error shows wrong url
  _url = "http://localhost:3000/api/register";
  _loginUrl = "http://localhost:3000/api/login"
  
  constructor( private _http: HttpClient, private _router: Router) { }

  // used in createuser.component
  createUser(userData){
    return this._http.post<any>(this._url, userData);
  }

  // used in login.component
  loginUser(user){
    return this._http.post<any>(this._loginUrl, user)
  }

  // used in register.component
  enroll(user : User){
    return this._http.post<any>(this._url, user)
      .pipe(catchError(this.errorHandler))
  }

  errorHandler(error:HttpErrorResponse){
    return throwError(error)
  }

  loggedIn(){
    return !!localStorage.getItem('token')
  }

  getToken(){
    return localStorage.getItem('token')
  }

  logoutUser(){
    localStorage.removeItem('token');
    this._router.navigate(['/events'])
  }
}
