// custom validators
import { AbstractControl, ValidatorFn } from '@angular/forms';

// custom validators without parameters
// export function forbiddenNameValidator(control: AbstractControl): { [key: string]: any } | null {
//     const forbidden = /admin/.test(control.value);
//     return forbidden ? { 'forbiddenName': { value: control.value } } : null;
// }

// custom validators with parameters
// function which returns validator function
// factory function
export function forbiddenNameValidator(forbiddenName: RegExp): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
        const forbidden = forbiddenName.test(control.value);
        return forbidden ? { 'forbiddenName': { value: control.value } } : null;
    }   
}